require 'rails_helper'

RSpec.describe Post, type: :model do
  #pending "add some examples to (or delete) #{__FILE__}"

  describe "model creation" do
    it "validating info" do
      post = Post.new
      post_with_blank_fields = Post.new( title: "", description: "  ")
      post_with_valid_info = Post.new( title: "title", description: "description")
      
      expect(post.valid?).to be(false)
      expect(post_with_blank_fields.valid?).to be(false)
      expect(post_with_valid_info.valid?).to be(true)
    end
  end
end
